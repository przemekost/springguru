package com.przemyslawostrouch.springguru.repository;

import com.przemyslawostrouch.springguru.model.Author;
import org.springframework.data.repository.CrudRepository;

public interface AuthorRepository extends CrudRepository<Author, Long>{
}
