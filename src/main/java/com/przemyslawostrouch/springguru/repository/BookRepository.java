package com.przemyslawostrouch.springguru.repository;

import com.przemyslawostrouch.springguru.model.Book;
import org.springframework.data.repository.CrudRepository;

public interface BookRepository extends CrudRepository<Book, Long> {

    
}
